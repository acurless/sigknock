CC = gcc
CFLAGS = -Wall -Wextra -O2 -fPIC
LINK = -static

OBJECT_DIR = .
INCLUDE =
SRC = $(wildcard *.c)

OBJ = $(patsubst %.c,$(OBJECT_DIR)/%.o,$(SRC))

ARTIFACT = irqknock

all: src

$(OBJECT_DIR)/%.o: %.c
	$(CC) -c $(INCLUDE) -o $@ $< $(CFLAGS)

$(ARTIFACT): $(OBJ)
	$(CC) -o $@ $^ $(LINK)

init:
	@mkdir -p $(OBJECT_DIR)

src: init $(ARTIFACT)
	docker build -t irqknock:latest .

run:
	docker run -it irqknock:latest

clean:
	rm -rf *.o $(ARTIFACT)

docker: all
	docker build -t irqknock:latest .
