#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

#define FLAG "WPI{1RQM@St3R}"

static volatile int state = 0;

void handler(int signum)
{
    printf("Got signal %d\n", signum);
    switch (signum)
    {
    case SIGINT:
        if (state == 0)
        {
            state = 1;
        }
        else
        {
            state = 0;
        }
        break;
    case SIGQUIT:
        if (state == 1)
        {
            state = 2;
        }
        else
        {
            state = 0;
        }
        break;
    case SIGSEGV:
        if (state == 2)
        {
            state = 3;
        }
        else
        {
            state = 0;
        }
        break;
    case SIGPIPE:
        if (state == 3)
        {
            state = 4;
        }
        else
        {
            state = 0;
        }
        break;
    case SIGCHLD:
        if (state == 4)
        {
            state = 5;
        }
        else
        {
            state = 0;
        }
        break;
    default:
        state = 0;
        break;
    }

    printf("State advanced to %d\n", state);
    if (state == 5)
    {
        printf("%s\n", FLAG);
        exit(1);
    }
}

int main(void)
{
    printf("I am pid: %d\n", getpid());
    signal(SIGINT, handler);
    signal(SIGQUIT, handler);
    signal(SIGSEGV, handler);
    signal(SIGPIPE, handler);
    signal(SIGCHLD, handler);

    while (1)
    {
        sleep(1);
    }
    return 0;
}
