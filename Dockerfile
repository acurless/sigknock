FROM alpine:latest
MAINTAINER Adrian Curless (awcurless@wpi.edu)

ENV UID=100
ENV USER=wpictf

RUN adduser -S ${USER} -u ${UID}

COPY irqknock /root/irqknock
RUN install -o ${UID} -g ${UID} -m 0100 /root/irqknock /usr/bin/irqknock
COPY init /bin/init_d
COPY solver.sh /root/solver.sh

RUN chown ${USER} /usr/bin/irqknock
RUN chown ${USER} /bin/init_d

USER wpictf

ENTRYPOINT /bin/init_d
