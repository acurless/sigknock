#!/bin/sh

pid=$1

kill -SIGINT $pid
sleep 1
kill -SIGQUIT $pid
sleep 1
kill -SIGSEGV $pid
sleep 1
kill -SIGPIPE $pid
sleep 1
kill -SIGCHLD $pid
