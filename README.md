# irqknock #
Port knocking but for software interrupts.

author: acurless
description: Random sequences are too hard to remember, always protect your data
with well-defined sequences.

General approach:
    See solver.sh for the exact solution.
